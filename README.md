# Semáforo Centros Educativos Área Sanitaria de Santiago

Track Covid-19 cases in schools from Galicia

Main data stored in CSV file under data/definitivo.csv and is updated from Tuesday to Saturday.

Please mention us like @JKniffki and/or names in Twitter if you find this useful :blush:

If you find a bug we would like to know it :pray:

Mainly maintained by:

J. Kniffki @[JKniffki](https://twitter.com/JKniffki)

Any contributions are welcomed!

You can find the report (in spanish) in this [link](https://kifirifis.github.io/post/2020-11-01-covid-19-area-sanitaria-de-santiago/).

------------------------------
Seguimiento de casos Covid-19 en Centros Educativos de Galicia.

La lista de positivos en Centros Educativos de Galicia con fecha y coordenadas se encuentra en el archivo data/definitivo.csv 

El archivo se actualiza de martes a sábado. 

Si encuentras algún error nos encantaría saberlo :pray: 

Cualquier colaboración es bienvenida. :ok_hand: :blush:

Este repositorio está mantenido por @[JKniffki](https://twitter.com/JKniffki).

El reporte del Área Sanitaria de Santiago puedes encontrarlo en este [enlace](https://kifirifis.github.io/post/2020-11-19-covid-19-area-sanitaria-de-santiago/).

----------------------------

